package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import com.tsc.skuschenko.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    private String projectId;

    @Nullable
    private String status = Status.NOT_STARTED.getDisplayName();

}
