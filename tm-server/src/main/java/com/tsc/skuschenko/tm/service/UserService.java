package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.*;
import com.tsc.skuschenko.tm.exception.entity.user.EmailExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.LoginExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserRepository userRepository;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final Role role
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        @NotNull final User user = create(login, password);
        user.setRole(role.getDisplayName());
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        return userRepository.findByEmail(email);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String userId, @Nullable final String firstName,
            @Nullable final String lastName, @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
