package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.api.service.IBusinessService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {
    @NotNull
    final IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(
            @NotNull final IBusinessRepository<E> entityRepository
    ) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @NotNull
    @Override
    public E changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(status.getDisplayName());
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final E entity =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(() -> new EntityNotFoundException(
                                        this.getClass().getSimpleName()
                                )
                        );
        entity.setStatus(status.getDisplayName());
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final E entity =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(() -> new EntityNotFoundException(
                                this.getClass().getSimpleName()));
        entity.setStatus(status.getDisplayName());
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityRepository.clear(userId);
    }

    @NotNull
    @Override
    public E completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setStatus(Status.COMPLETE.getDisplayName());
        entity.setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setStatus(Status.COMPLETE.getDisplayName());
        entity.setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E completeByName(@NotNull final String userId,
                            @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final E entity =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(() -> new EntityNotFoundException(
                                this.getClass().getSimpleName()));
        entity.setStatus(Status.COMPLETE.getDisplayName());
        entity.setDateFinish(new Date());
        return entity;
    }

    @Nullable
    @Override
    public List<E> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<E> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        return entityRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entityRepository.findAll(userId);
    }

    @Nullable
    @Override
    public E findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public E findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return entityRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public E removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public E removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return entityRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.removeOneByName(userId, name);
    }

    @NotNull
    @Override
    public E startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final E entity =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(() -> new EntityNotFoundException(
                                this.getClass().getSimpleName()));
        entity.setStatus(Status.IN_PROGRESS.getDisplayName());
        entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    @Override
    public E updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
