package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.model.Session;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class SessionService implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(
            @NotNull final IServiceLocator serviceLocator,
            @NotNull final ISessionRepository sessionRepository
    ) {
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    @Nullable
    public User checkDataAccess(
            @Nullable final String login,
            @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent()) return null;
        if (!Optional.ofNullable(password).isPresent()) return null;
        @Nullable final User user =
                serviceLocator.getUserService().findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return null;
        @Nullable final String secret =
                serviceLocator.getPropertyService().getPasswordSecret();
        @Nullable final Integer iteration
                =  serviceLocator.getPropertyService().getPasswordIteration();
        @Nullable final String passwordHash =
                HashUtil.salt(secret,iteration,password);
        if (!Optional.ofNullable(passwordHash).isPresent() ||
                !passwordHash.equals(user.getPasswordHash())) return null;
        return user;
    }

    @Override
    public void close(@NotNull final Session session)
            throws AccessForbiddenException {
        sessionRepository.remove(session);
    }

    @Nullable
    @Override
    public Session open(
            @Nullable final String login, @Nullable final String password
    ) {
        @Nullable final User user = checkDataAccess(login, password);
        Optional.ofNullable(user).orElseThrow(AccessForbiddenException::new);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (!Optional.ofNullable(session).isPresent()) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService =
                serviceLocator.getPropertyService();
        @Nullable final String salt = propertyService.getSessionSalt();
        if (!Optional.ofNullable(salt).isPresent()) return null;
        @Nullable final String cycle = propertyService.getSessionCycle();
        if (!Optional.ofNullable(cycle).isPresent()) return null;
        final int iteration = Integer.parseInt(cycle);
        @Nullable final String signature =
                SignatureUtil.sign(session, salt, iteration);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@Nullable final Session session)
            throws AccessForbiddenException {
        Optional.ofNullable(session)
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getSignature())
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getUserId())
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getTimestamp())
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final Session tempSession = session.clone();
        Optional.ofNullable(tempSession)
                .orElseThrow(AccessForbiddenException::new);
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session signSession = sign(tempSession);
        Optional.ofNullable(signSession)
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final String signatureTarget = signSession.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        Optional.ofNullable(sessionRepository.findById(session.getId()))
                .orElseThrow(AccessForbiddenException::new);
    }

    @Override
    public void validate(@Nullable Session session, @Nullable Role role)
            throws AccessForbiddenException {
        Optional.ofNullable(role)
                .orElseThrow(AccessForbiddenException::new);
        validate(session);
        @Nullable final String userId = session.getUserId();
        Optional.ofNullable(userId)
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final User user =
                serviceLocator.getUserService().findById(userId);
        Optional.ofNullable(user)
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(user.getRole())
                .orElseThrow(AccessForbiddenException::new);
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

}
