package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.enumerated.Sort;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    @Nullable IProjectService projectService;

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @WebMethod
    @NotNull
    public Project addProject(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description")
            @NotNull final String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.add(session.getUserId(), name, description);
    }

    @WebMethod
    @NotNull
    public Project changeProjectStatusById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.changeStatusById(
                session.getUserId(), id, Status.valueOf(status)
        );
    }

    @WebMethod
    @NotNull
    public Project changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.changeStatusByIndex(
                session.getUserId(), index, Status.valueOf(status)
        );
    }

    @WebMethod
    @NotNull
    public Project changeProjectStatusByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.changeStatusByName(
                session.getUserId(), name, Status.valueOf(status)
        );
    }

    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public void clearProjects(@WebParam(name = "session", partName = "session")
                              @NotNull final Session session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService()
                .clearProjects(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Project completeProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.completeById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public Project completeProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.completeByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public Project completeProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.completeByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public Project deleteProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "projectId", partName = "projectId")
            @NotNull final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().deleteProjectById(
                session.getUserId(), projectId
        );
    }

    @WebMethod
    @Nullable
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    public List<Project> findProjectAllSort(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "sort", partName = "sort")
            @Nullable final String sort
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Sort sortType = Sort.valueOf(sort);
        return projectService.findAll(
                session.getUserId(), sortType.getComparator()
        );
    }

    @WebMethod
    @Nullable
    public Project findProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findOneById(session.getUserId(), id);
    }

    @WebMethod
    @Nullable
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Nullable
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @Nullable
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Nullable
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public Project startProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public Project startProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public Project startProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "description", partName = "description")
            @Nullable final String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateOneById(
                session.getUserId(), id, name, description
        );
    }

    @WebMethod
    @NotNull
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final Session session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "description", partName = "description")
            @Nullable final String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateOneByIndex(
                session.getUserId(), index, name, description
        );
    }

}
