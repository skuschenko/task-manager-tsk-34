package com.tsc.skuschenko.tm.enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN("Administrator"),

    USER("User");

    @NotNull
    public final String displayName;

}
