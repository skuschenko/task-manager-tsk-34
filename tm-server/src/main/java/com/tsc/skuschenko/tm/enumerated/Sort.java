package com.tsc.skuschenko.tm.enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsc.skuschenko.tm.comparator.ComparatorByCreated;
import com.tsc.skuschenko.tm.comparator.ComparatorByDateStart;
import com.tsc.skuschenko.tm.comparator.ComparatorByName;
import com.tsc.skuschenko.tm.comparator.ComparatorByStatus;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

@Getter
public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final Comparator comparator;

    @NotNull
    private final String displayName;

    Sort(
            @NotNull final String displayName,
            @NotNull final Comparator comparator
    ) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
