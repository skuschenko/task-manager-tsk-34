package com.tsc.skuschenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    void exit();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
