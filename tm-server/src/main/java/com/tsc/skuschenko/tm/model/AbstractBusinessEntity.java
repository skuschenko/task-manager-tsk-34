package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity {

    @Nullable
    private Date created = new Date();

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date dateStart;

    @Nullable
    private String description = "";

    @Nullable
    private String name = "";

    @Nullable
    private String status = Status.NOT_STARTED.getDisplayName();

    @Nullable
    private String userId;

    public String getUserId() {
        if (userId == null || userId.isEmpty()) {
            throw new AccessDeniedException();
        }
        return userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
