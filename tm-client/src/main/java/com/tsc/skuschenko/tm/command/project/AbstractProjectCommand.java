package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.Status;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected String readProjectStatus() {
        showParameterInfo("status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String status = TerminalUtil.nextLine();
        return status;
    }

    protected void showProject(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
        System.out.println("START DATE: " + project.getDateStart());
        System.out.println("END DATE: " + project.getDateFinish());
    }

}
