package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.endpoint.Status;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected String readTaskStatus() {
        showParameterInfo("status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String status = TerminalUtil.nextLine();
        return status;
    }

    protected void showTask(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus());
        System.out.println("START DATE: " + task.getDateStart());
        System.out.println("END DATE: " + task.getDateFinish());
    }
}
