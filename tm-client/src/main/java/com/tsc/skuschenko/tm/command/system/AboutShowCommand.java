package com.tsc.skuschenko.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class AboutShowCommand extends AbstractCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "about";

    @NotNull
    private static final String NAME = "about";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        @NotNull final IPropertyService service =
                serviceLocator.getPropertyService();
        System.out.println("AUTHOR: " + Manifests.read("developer"));
        System.out.println("EMAIL: " + Manifests.read("email"));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
