package com.tsc.skuschenko.tm.command;

import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.endpoint.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public abstract String name();

    public Role[] roles() {
        return null;
    }

    public void setServiceLocator(
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    protected void showOperationInfo(@NotNull final String info) {
        System.out.println("[" + info.toUpperCase() + "]");
    }

    protected void showParameterInfo(@NotNull final String info) {
        System.out.println("ENTER " + info.toUpperCase() + ":");
    }

}
