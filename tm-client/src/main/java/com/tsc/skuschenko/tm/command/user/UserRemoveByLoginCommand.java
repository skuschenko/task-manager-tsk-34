package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Role;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "remove user by login";

    @NotNull
    private static final String NAME = "remove-user-by-login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showParameterInfo("login");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().removeUserByLogin(session, password);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
