package com.tsc.skuschenko.tm.command.data;

import com.tsc.skuschenko.tm.endpoint.Role;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "load data from json file";

    @NotNull
    private static final String NAME = "data-load-json";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        serviceLocator.getDataEndpoint().loadDataJsonFasterXmlCommand(session);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
