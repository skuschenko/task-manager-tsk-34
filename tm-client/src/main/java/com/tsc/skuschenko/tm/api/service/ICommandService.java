package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand abstractCommand);

    @NotNull
    Collection<AbstractCommand> getArgs();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getListArgumentName();

    @NotNull
    Collection<String> getListCommandNames();

}
