package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "show all projects";

    @NotNull
    private static final String NAME = "project-list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("sort");
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Project> projects = new ArrayList<>();

        if (sort.isEmpty()) {
            projects = serviceLocator.getProjectEndpoint()
                    .findProjectAll(session);
        } else {
            projects = serviceLocator.getProjectEndpoint()
                    .findProjectAllSort(session, sort);
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
